function serialjsonnum (text: string, num: number) {
    serial.writeString("\"")
    serial.writeString(text)
    serial.writeString("\"")
    serial.writeString(": \"")
    serial.writeNumber(num)
    serial.writeString("\", ")
}
function serialjsontext (text: string, text2: string) {
    serial.writeString("\"")
    serial.writeString(text)
    serial.writeString("\"")
    serial.writeString(": ")
    serial.writeString("\"")
    serial.writeString(text2)
    serial.writeString("\"")
}
basic.showString("v4")
basic.forever(function () {
    weatherbit.startWeatherMonitoring()
    weatherbit.startWindMonitoring()
    weatherbit.startRainMonitoring()
    while (true) {
        serial.writeString("{ ")
        serialjsonnum("temperature", weatherbit.temperature() / 100)
        serialjsonnum("pressure", weatherbit.pressure() / 256 / 100)
        serialjsonnum("humidity", weatherbit.humidity() / 1024)
        serialjsonnum("altitude", weatherbit.altitude())
        serialjsonnum("rain", weatherbit.rain())
        serialjsonnum("soil-temp", weatherbit.soilTemperature() / 100)
        serialjsonnum("soil-moisture", weatherbit.soilMoisture() / 1023)
        serialjsonnum("wind-speed", weatherbit.windSpeed())
        serialjsontext("wind direction", weatherbit.windDirection())
        serial.writeLine(" }")
        basic.pause(1000)
    }
})
