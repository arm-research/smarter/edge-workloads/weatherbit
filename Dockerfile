FROM python:3.7.3-alpine3.9

WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
COPY weatherbit.py /usr/src/app/weatherbit.py

ENTRYPOINT [ "/usr/local/bin/python3" ]
CMD [ "/usr/src/app/weatherbit.py" ]
