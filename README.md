# weatherbit

Support reading data stream from weatherbit

This code expects the weatherbit to be plugged into USB
and assumes it shows up as /dev/ttyACM0 by default (this
can be changed with environment variables)

It will output values to MQTT to fluent-bit under the topic /demo/weatherbit
(also settable via environment variables)

At the moment, its taking measurements once every second.
Values are output as strings to avoid schema issues with InfluxDB.

# Flashing your weather:bit

We used MakeCode to build the firmware for the weather:bit.
You can use the javascript version in this repo or just copy
the compiled hex.
